<?php

namespace Drupal\domcs\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form that configures your_module settings.
 */
class CssSwitcherSettingForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'domcs.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'css_switcher_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Load the configuration entity.
    $config = $this->config('domcs.settings');

    $form['uploaded_css'] = [
      '#type' => 'managed_file',
      '#title' => $this->t('Css uploder'),
      '#description' => $this->t('Upload a file.'),
      '#default_value' => $config->get('uploaded_css'),
      '#upload_location' => 'public://domcs/files',
      '#upload_validators' => [
        'file_validate_extensions' => ['pdf doc docx png jpeg'],
      ],
    ];
    return parent::buildForm($form, $form_state);

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('domcs.settings')
      ->set('uploaded_css', $form_state->getValue('uploaded_css'))
      ->save();
  }

}
